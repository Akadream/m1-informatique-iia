# Comprendre Git

Git est un outil permettant la gestion de version. Cela signifie que vous allez à chaque nouvel envoi de votre code, créer une nouvelle version de celui-ci.
C'est un outil incontournable lorsque vous travaillez à plusieurs sur un projet. Il vous permet de récupérer facilement le code des autres membres et de le fusionner à votre code.

Cependant, vous devez faire attention si vous travaillez sur les mêmes fichiers que vos collègues, vous risquez de créer des conflits lorsque vous voudrez envoyer ou récupérer du code.
C'est pourquoi Git agit comme un arbre dans lequel vous pourrez ajouter des branches parallèle à la branche principale où vous pourrez faire toutes les modifications que vous voulez et les envoyer sur serveur sans risquer de créer des conflits.

Généralement, Git s'utilise avec un gestionnaire de dépôts tel que Gitlab, Github ou encore Bitbucket pour ne citer que les plus connus. Vous pourrez ainsi sauvegarder votre code sur leurs serveurs pour pouvoir le partager avec d'autres personnes.

### Les commandes de base
Voici quelques commandes de base à savoir pour utiliser correctement Git :

* `git add [files]` : Permet d'ajouter des fichiers au prochain commit. Remplacez [files] par les fichiers que vous voulez ajouter. Vous pouvez utiliser . pour ajouter tous les fichiers qui ont été modifiés depuis le dernier commit.
* `git commit` : Permet de créer une nouvelle version du code dans votre projet. Vous devez définir une description à votre commit. Cette description doit être simple est clair et doit préciser ce qui a été ajouté depuis le précédent commit. On peut utiliser l'argument `-m [description]` pour aller plus vite.
* `git push` : Cette commande permet d'envoyer votre code sur serveur. Si vous effectuer votre premier push, vous devez spécifier la branche dans laquelle vous voulez push. Par défaut, c'est la branche master. On fera donc `git push -u origin master`.
* `git fetch` : Permet de récupérer les objets et branches distantes depuis un autre dépôt.
* `git pull` : Permet de récupérer mettre à jours votre code selon le code du dépôt. Cela comprends également le code des autres branches.
* `git branch [nom]` : Cette commande permet de créer une nouvelle branche.
* `git checkout [nom]` : Si vous spécifiez le nom d'une branche, alors vous allez vous placer sur cette branche. Sinon, la commande affiche le nom de toutes les branches en mettant en évidance celle sur laquelle vous vous trouvez.
* `git merge [nom]` : Permet de fusionner la branche que vous spécifiez en paramètre avec celle sur laquelle vous vous trouvez actuellement. Il se peut qu'il y ai des conflits si plusieurs personnes modifient les mêmes lignes d'un même fichier. C'est pas censé arriver, mais sait-on jamais.
* `git status`: Affiche le status de votre dépôt local. La commande vous dira que tout est à jour si vous n'avez pas fait de modifications depuis votre dernier commit. Sinon, elle affichera tous les fichiers qui ont été modifiés.

C'est un peu barbare si vous ne connaissez pas git, mais vous verrez très vite en les utilisant qu'elles sont simple à utiliser et à retenir.