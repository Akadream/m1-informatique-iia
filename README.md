# M1 Informatique - IIA

Documentation pour le master 1 informatique.


### Pourquoi
L'alternance étant relativement intense (2 jours de journées complètes de cours) puis le travail, le temps de révision et de compréhension peut-être limité.
Ainsi, l'entraide peut-être une bonne réponse pour qu'on puisse tous comprendre les cours.

### Comment contribuer
Chacun va avoir ses forces et ses faiblesses sur les différentes matières. Si chacun complète la documentation présente ici, on pourra tous compléter nos connaissances et ça apaisera le rythme.

### Écrire de la documentation
Toutes les pages sont écrites en markdown. Vous trouverez assez simplement comment ça fonctionne (si vous allez sur la partie "modification de cette page" par exemple, vous verrez comment elle est construite).

Vous pouvez donc créer un nouveau fichier avec l'extension .md et écrire son contenu et c'est tout !